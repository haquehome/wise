const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
	mode: 'development',
	entry: "./src/index.ts",
	output: {
		filename: 'main.js',
		path: path.resolve(__dirname, 'public')
	},
	resolve: {
		extensions: ['.ts', '.js']
	},
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: ['ts-loader']
			},
			{
				test: /\.scss$/,
				exclude: path.resolve(__dirname, 'node_modules'),
				use: [
					'style-loader',
					'css-loader',
					'sass-loader'
				]
			},
		]
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './templates/index.html'
		})

	]
}