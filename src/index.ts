import './wise.scss'

interface Final {
	[key: number]: number
}

interface preFinal {
	factor: Final
	order: Final
}

interface Remainder {
	currentPackSize: number
	currentState: preFinal
	packSizes: number[]
	remainder: number
	startIndex: number
}

const filterExactTotals = (picks: Final[], orderAmount: number): Final[] => {
	return picks.filter(
		order => Object.entries(order).reduce((total, [size, qty]) => total + parseInt(size) * qty, 0) === orderAmount
	)
}

export const getPackSizesThatAreFactorsForRemainder = (remainder: number, packSizes: number[]): boolean => {
	let addMinToOrderAmount = false
	const packSizesCopy = [...packSizes].reverse()
	const findFactor = (remainder: number, packSizes: number[]) => {
		addMinToOrderAmount = packSizes.some(size => remainder % size === 0)
		if (!addMinToOrderAmount && packSizes.length > 0) {
			findFactor(remainder % packSizesCopy[0], packSizes.slice(1))
		}
	}
	findFactor(remainder, packSizesCopy)
	return !addMinToOrderAmount
}

export const getPackSizesWithinRangeOfOrder = (orderAmount: number, packSizes: number[]): number[] =>
	orderAmount < packSizes[0] ? [packSizes[0]] : packSizes.filter(packSize => packSize <= orderAmount)

export const checkIfMinPackSizeWillBeAdded = (orderAmount: number, packSizes: number[]): number => {
	const remainder = packSizes.reduceRight((orderAmount, pack) => orderAmount % pack, orderAmount)
	return remainder ? packSizes[0] - remainder : 0
}

const sortAndReverseArray = (array: number[]) => array.sort((a, b) => a - b).reverse()

const getComparisonValues = (order: number[]): [number, number, number[]] => [
	order.reduce((total: number, qty: number) => total + qty, 0),
	order.length,
	order
]

export const getOrder = (remainder: number, packSizes: number[]): Final => {
	let remainderLog: Remainder[] = []

	const remainderQueue = (remainder: number, packSize: number, packSizes: number[], currentState: any) => {
		const packSizesArrayIndex = packSizes.findIndex(size => size === packSize)
		const currentPackSize = packSizes[packSizesArrayIndex - 1]
		remainderLog.push({ currentPackSize, startIndex: packSizesArrayIndex, remainder, packSizes, currentState })
	}

	const iteratePacks = (packSizes: number[], remainder: number, currentState: Final = {}) => {
		let remainLoopUpdated = remainder
		const MIN_PACK_SIZE = packSizes[packSizes.length - 1]
		let orderCombinations: preFinal = { factor: {}, order: {} }
		for (let index = 0; index < packSizes.length; index++) {
			const [qty, remaining] = [Math.floor(remainLoopUpdated / packSizes[index]), remainLoopUpdated % packSizes[index]]
			remainLoopUpdated = remaining

			if (qty > 0) {
				orderCombinations = {
					...orderCombinations,
					...{ order: { ...orderCombinations.order, [packSizes[index]]: qty } }
				}
			}

			if (packSizes.length > 0 && remaining > 0 && remaining < MIN_PACK_SIZE) {
				const newOrder = {
					...orderCombinations.order,
					[MIN_PACK_SIZE]: (orderCombinations.order[MIN_PACK_SIZE] ?? 0) + 1
				}
				orderCombinations.order = newOrder
				break
			}

			if (remaining > 0 && packSizes.length > 1 && index + 1 < packSizes.length && remaining > MIN_PACK_SIZE) {
				if (Object.keys(orderCombinations).length === 0) orderCombinations = { order: {}, factor: {} }
				orderCombinations.order = { ...orderCombinations.order, ...currentState }
				remainderQueue(remaining, packSizes[index + 1], packSizes, { ...orderCombinations })
			}
		}
		const factorials = packSizes.find(size => remainder % size === 0)
		if (factorials) {
			const qty = Math.floor(remainder / factorials)
			if (parent !== undefined) {
				orderCombinations.factor = { [factorials]: qty }
			}
		}

		return orderCombinations
	}

	const compare = (currentBest: preFinal, newOrder: Final) => {
		const [newOrderTotal, newOrderLength] = getComparisonValues(Object.values(newOrder))
		const [currentBestTotal, currentBestLength] = getComparisonValues(Object.values(currentBest.order))

		if (newOrderTotal < currentBestTotal) currentBest.order = newOrder
		if (newOrderTotal === currentBestTotal && newOrderLength < currentBestLength) currentBest.order = newOrder
	}

	const runRemainder = (finalOrderCombinations: preFinal) => {
		if (remainderLog.length === 0) return
		const remainderLogCopy = [...remainderLog]
		remainderLog = Array(0)
		for (let i = 0; i < remainderLogCopy.length; i++) {
			const { packSizes, remainder, startIndex, currentState } = remainderLogCopy[i]
			const rangeSizes = packSizes.slice(startIndex)
			const orderCombinations = iteratePacks(rangeSizes, remainder, currentState.order)
			if (Object.keys(orderCombinations.factor).length > 0) orderCombinations.order = orderCombinations.factor
			const tempOrder = {
				...currentState.order,
				...(orderCombinations.order ?? Object.values(orderCombinations.order)[0])
			}
			compare(finalOrderCombinations, tempOrder)
		}

		runRemainder(finalOrderCombinations)
	}
	const getPacks = (remainder: number, packSizes: number[]): Final => {
		const finalOrderCombinations = iteratePacks(packSizes, remainder)
		runRemainder(finalOrderCombinations)

		return finalOrderCombinations.order
	}

	return getPacks(remainder, packSizes)
}

export const order = (orderAmount: number, packSizes: number[]): any => {
	const rangePackSizes = getPackSizesWithinRangeOfOrder(orderAmount, packSizes)
	const addMinToTotol = getPackSizesThatAreFactorsForRemainder(orderAmount, rangePackSizes)
	let updatedOrderAmount = orderAmount

	if (addMinToTotol) {
		const addToPackSizeAmount = checkIfMinPackSizeWillBeAdded(orderAmount, rangePackSizes)
		updatedOrderAmount = orderAmount + (addToPackSizeAmount ?? 0)
		const nextPackSize = packSizes[rangePackSizes.length]
		if (nextPackSize !== undefined && updatedOrderAmount >= nextPackSize) {
			rangePackSizes.push(nextPackSize)
			updatedOrderAmount = nextPackSize
		}
	}

	const packSizePicksForOrder: Final[] = sortAndReverseArray(rangePackSizes).map((pack, index) =>
		getOrder(updatedOrderAmount, rangePackSizes.slice(index))
	)

	const selectBestOrderCombination = (picks: Final[]) => {
		let best: Final = { 0: -1 }
		let [bestTotal, , bestValues] = getComparisonValues(Object.values(best))

		for (const order of picks) {
			const [orderTotal, orderLength, orderValues] = getComparisonValues(Object.values(order))
			if (orderTotal < bestTotal || bestTotal === -1) {
				;[best, bestTotal, bestValues] = [order, orderTotal, orderValues]
			}
			if (orderTotal === bestTotal && orderLength < bestValues.length) {
				;[best, bestValues] = [order, orderValues]
			}
		}

		return best
	}
	const filteredExactTotals = filterExactTotals(packSizePicksForOrder, orderAmount)
	const bestOrder = selectBestOrderCombination(
		filteredExactTotals.length > 0 ? filteredExactTotals : packSizePicksForOrder
	)

	const bestOrderTotal = Object.entries(bestOrder).reduce((total, [size, qty]) => total + parseInt(size) * qty, 0)

	const MIN_PACK_SIZE = rangePackSizes[rangePackSizes.length - 1]

	if (bestOrderTotal !== orderAmount && bestOrderTotal - MIN_PACK_SIZE > orderAmount && bestOrder !== undefined) {
		if (Object.values(bestOrder)[0] === 1) {
			const [size] = Object.keys(bestOrder)
			delete bestOrder[parseInt(size)]
		}
	}

	return bestOrder
}

const input = document.getElementsByTagName('input')[0]
const dataElement = document.getElementById('data')
input?.addEventListener('keydown', e => {
	const input = e.target as HTMLInputElement
	const value = parseInt(input.value)
	const packSizes = [
		[40, 250, 300, 500, 4000, 5000],
		[40, 300, 500, 4000, 5000],
		[3, 43, 86, 500, 1000, 4000, 5000],
		[50, 300, 500, 5000], // 3
		[50, 300, 500, 4000, 5000],
		[3, 4, 43, 1000],
		[18, 300, 500, 5000],
		[10, 20, 50, 80],
		[4, 25, 50, 400, 500],
		[4, 25, 46, 477, 5653], // 9
		[250, 500, 1000, 4000, 5000]
	]
	if (e.keyCode === 13 && Number.isInteger(value) && value > 0) {
		const defaultPackSizes: number[] = packSizes[6]
		const packs = order(value, defaultPackSizes)

		dataElement && (dataElement.innerHTML = '')

		for (const key of Object.keys(packs)) {
			const spanElement = document.createElement('span')
			const spanElement2 = document.createElement('span')
			spanElement.appendChild(document.createTextNode(key))
			spanElement2.appendChild(document.createTextNode(packs[key].toString()))
			dataElement?.appendChild(spanElement)
			dataElement?.appendChild(spanElement2)
		}

		const idElementLog: HTMLElement | null = document.getElementById('log')
		input.value = ''
		idElementLog && (idElementLog.innerHTML = '')

		const logElementHeaderText = document.createElement('h4')
		logElementHeaderText.appendChild(document.createTextNode('log'))
		idElementLog?.appendChild(logElementHeaderText)

		const orderAmountElement = document.createElement('h5')
		const orderAmountSpanElement = document.createElement('span')
		orderAmountSpanElement.appendChild(document.createTextNode(`${value}`))
		orderAmountElement.appendChild(document.createTextNode(`Order amount entered`))
		orderAmountElement.appendChild(orderAmountSpanElement)
		idElementLog?.appendChild(orderAmountElement)

		const packsElement = document.createElement('h4')
		packsElement.appendChild(document.createTextNode('Pack Sizes'))
		idElementLog?.appendChild(packsElement)

		const packsDiv = document.createElement('div')
		packsDiv.setAttribute('id', 'packSizes')
		defaultPackSizes.forEach((v: number) => {
			const packsSpan = document.createElement('span')
			packsSpan.appendChild(document.createTextNode(v.toString()))
			packsDiv.appendChild(packsSpan)
		})
		idElementLog?.appendChild(packsDiv)
	}
})
