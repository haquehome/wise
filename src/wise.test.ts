/**
 * @jest-environment jsdom
 */
import { order } from '.'

describe('wise tests', () => {
	it('packsizes', () => {
		const packSizes = [10, 30, 500, 1000, 4000, 5000]
		expect(order(999, packSizes)).toEqual({ '1000': 1 })
	})

	it('packsizes', () => {
		const packSizes = [100, 300, 500, 5000]
		expect(order(600, packSizes)).toEqual({ '300': 2 })
	})

	it('packsizes', () => {
		const packSizes = [40, 250, 500]
		expect(order(991, packSizes)).toEqual({ '500': 2 })
	})

	it('packsizes', () => {
		const packSizes = [3, 43, 86, 300, 500, 5000]
		expect(order(5600, packSizes)).toEqual({ '5000': 1, '300': 2 })
	})

	it('packsizes', () => {
		const packSizes = [40, 250, 500, 1000, 4000, 5000]
		expect(order(12000, packSizes)).toEqual({ '4000': 3 })
	})

	it('packsizes', () => {
		const packSizes = [10, 30, 150, 500, 1000, 4000, 5000]
		expect(order(180, packSizes)).toEqual({ '30': 1, '150': 1 })
	})

	it('packsizes 14749', () => {
		const packSizes = [3, 43, 86, 500, 1000, 4000, 5000]
		expect(order(14749, packSizes)).toEqual({ '43': 1, '86': 171 })
	})

	it('get packs for 17925 with pack', () => {
		// const packSizes = [2, 500, 1023, 4000, 5000]
		const packSizes = [250, 500, 1000, 4000, 5000]
		expect(order(11999, packSizes)).toEqual({ '4000': 3 })
	})

	it('get packs for 1', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(1, packSizes)).toEqual({ '250': 1 })
	})

	it('get packs for 250', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(250, packSizes)).toEqual({ '250': 1 })
	})

	it('get packs for 251', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(251, packSizes)).toEqual({ '500': 1 })
	})

	it('get packs for 500', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(500, packSizes)).toEqual({ '500': 1 })
	})

	it('get packs for 501', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(501, packSizes)).toEqual({ '500': 1, '250': 1 })
	})

	it('get packs for 750', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(750, packSizes)).toEqual({ '250': 1, '500': 1 })
	})

	it('get packs for 751', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(751, packSizes)).toEqual({ '1000': 1 })
	})

	it('get packs for 805', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(805, packSizes)).toEqual({ '1000': 1 })
	})

	it('get packs for 12,001', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(12001, packSizes)).toEqual({ '5000': 2, '2000': 1, '250': 1 })
	})

	it('get packs for 12,001 packSizes', () => {
		const packSizes = [250, 500, 1000, 4000, 5000]
		expect(order(12001, packSizes)).toEqual({ '4000': 3, '250': 1 })
	})

	it('get packs for 15,001', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(15001, packSizes)).toEqual({ '5000': 3, '250': 1 })
	})

	it('get packs for 23,751', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(23751, packSizes)).toEqual({ '5000': 4, '2000': 2 })
	})

	it('get packs for 80,751', () => {
		const packSizes = [250, 500, 1000, 6000, 15000]
		expect(order(81751, packSizes)).toEqual({ '1000': 1, '15000': 5, '6000': 1 })
	})

	it('get packs for 80,751', () => {
		const packSizes5 = [80, 500, 1000, 4000, 5000]
		expect(order(12001, packSizes5)).toEqual({ '4000': 3, '80': 1 })
	})

	it('get packs for 1,765,654,000,154,300', () => {
		const packSizes = [250, 500, 1000, 2000, 5000]
		expect(order(1765654000154300, packSizes)).toEqual({ '5000': 353130800030, '2000': 2, '500': 1 })
	})
})
